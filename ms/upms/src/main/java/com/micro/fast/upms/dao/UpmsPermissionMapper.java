package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsPermission;

public interface UpmsPermissionMapper extends SsmMapper<UpmsPermission,Integer> {
    int deleteByPrimaryKey(Integer id);

    int insert(UpmsPermission record);

    int insertSelective(UpmsPermission record);

    UpmsPermission selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UpmsPermission record);

    int updateByPrimaryKey(UpmsPermission record);
}