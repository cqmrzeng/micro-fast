package com.micro.fast.upms.service.impl;

import com.micro.fast.common.service.impl.SsmServiceImpl;
import com.micro.fast.upms.dao.UpmsUserPermissionMapper;
import com.micro.fast.upms.pojo.UpmsUserPermission;
import com.micro.fast.upms.service.UpmsUserPermissionService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author lsy
*/
@Service
public class UpmsUserPermissionServiceImpl  extends SsmServiceImpl<UpmsUserPermission,Integer,UpmsUserPermissionMapper>
implements UpmsUserPermissionService<UpmsUserPermission,Integer>,InitializingBean {

  @Autowired
  private UpmsUserPermissionMapper mapper;

  /**
  * 在这个bean初始化完成后覆盖父类的mapper
  */
  @Override
  public void afterPropertiesSet() throws Exception {
    super.setMapper(this.mapper);
  }
}
