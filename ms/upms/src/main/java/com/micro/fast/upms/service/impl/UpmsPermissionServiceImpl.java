package com.micro.fast.upms.service.impl;

import com.micro.fast.common.service.impl.SsmServiceImpl;
import com.micro.fast.upms.dao.UpmsPermissionMapper;
import com.micro.fast.upms.pojo.UpmsPermission;
import com.micro.fast.upms.service.UpmsPermissionService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author lsy
*/
@Service
public class UpmsPermissionServiceImpl  extends SsmServiceImpl<UpmsPermission,Integer,UpmsPermissionMapper>
implements UpmsPermissionService<UpmsPermission,Integer>,InitializingBean {

  @Autowired
  private UpmsPermissionMapper mapper;

  /**
  * 在这个bean初始化完成后覆盖父类的mapper
  */
  @Override
  public void afterPropertiesSet() throws Exception {
    super.setMapper(this.mapper);
  }
}
