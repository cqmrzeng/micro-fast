package com.micro.fast.upms.controller;

import com.micro.fast.boot.starter.common.response.BaseConst;
import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.upms.pojo.UpmsUserOrganization;
import com.micro.fast.upms.service.UpmsUserOrganizationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
*
* @author lsy
*/
@Api("upmsUserOrganization")
@RestController
@RequestMapping("/upmsUserOrganization")
public class UpmsUserOrganizationController {

  @Autowired
  private UpmsUserOrganizationService<UpmsUserOrganization,Integer> upmsUserOrganizationService;

  @ApiOperation("添加信息")
  @PostMapping
  public ServerResponse addUpmsUserOrganization(@Valid UpmsUserOrganization upmsUserOrganization) throws BindException {
    return  upmsUserOrganizationService.add(upmsUserOrganization);
  }

  @ApiOperation("根据id查询详细信息")
  @GetMapping("/{id}")
  public ServerResponse getUpmsUserOrganizationById(
  @PathVariable(value = "id") @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请传入id") String id){
    return upmsUserOrganizationService.getById(Integer.valueOf(id));
  }

  @ApiOperation("根据条件分页查询")
  @GetMapping
  public ServerResponse getUpmsUserOrganizationByCondition(UpmsUserOrganization upmsUserOrganization,
                                             @RequestParam(defaultValue = "1",required = false) int pageNum,
                                             @RequestParam(defaultValue = "10",required = false)int pageSize,
                                             @RequestParam(required = false) String orderBy){
    return upmsUserOrganizationService.getByCondition(upmsUserOrganization,pageNum,pageSize,orderBy);
  }

  @ApiOperation("修改信息")
  @PutMapping("/{id}")
  public ServerResponse updateUpmsUserOrganization(UpmsUserOrganization upmsUserOrganization,
                                     @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请传入id") String id){
    upmsUserOrganization.setUserOrganizationId(Integer.valueOf(id));
    return upmsUserOrganizationService.update(upmsUserOrganization);
  }

  @ApiOperation("根据id删除，传入数组")
  @DeleteMapping
  public ServerResponse deleteUpmsUserOrganization(@NotEmpty(message = BaseConst.BASEMSG_PREFIX+"请传入ids") List<Integer> ids){
    return upmsUserOrganizationService.deleteByIds(ids);
  }
}
