package com.micro.fast.upms.pojo;

import com.micro.fast.boot.starter.common.response.BaseConst;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

public class UpmsUser implements Serializable {

    private Integer id;
    /**
     * 用户名必须为6-16位字母或数字的组合
     */
    @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请按要求输入用户名")
    @Pattern(regexp = "^[a-z,A-Z,0-9]{6,16}$",message = BaseConst.BASEMSG_PREFIX+"请按要求输入用户名")
    private String username;

    /**
     * 密码必须为6-16位字母或数字的组合
     */
    @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请按要求输入密码")
    @Pattern(regexp = "^[a-z,A-Z,0-9]{6,16}$",message = BaseConst.BASEMSG_PREFIX+"请按要求输入密码")
    private String password;

    private String salt;

    private String realname;

    private String avatar;

    private String phone;

    @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请按要求输入邮箱")
    @Email(message = BaseConst.BASEMSG_PREFIX+"邮箱格式不正确")
    private String email;

    private Byte sex;

    private Byte locked;

    private Long ctime;

    private static final long serialVersionUID = 1L;

    public UpmsUser(Integer id, String username, String password, String salt, String realname, String avatar, String phone, String email, Byte sex, Byte locked, Long ctime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.realname = realname;
        this.avatar = avatar;
        this.phone = phone;
        this.email = email;
        this.sex = sex;
        this.locked = locked;
        this.ctime = ctime;
    }

    public UpmsUser() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname == null ? null : realname.trim();
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Byte getSex() {
        return sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    public Byte getLocked() {
        return locked;
    }

    public void setLocked(Byte locked) {
        this.locked = locked;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", salt=").append(salt);
        sb.append(", realname=").append(realname);
        sb.append(", avatar=").append(avatar);
        sb.append(", phone=").append(phone);
        sb.append(", email=").append(email);
        sb.append(", sex=").append(sex);
        sb.append(", locked=").append(locked);
        sb.append(", ctime=").append(ctime);
        sb.append("]");
        return sb.toString();
    }
}