package com.micro.fast.upms.service.impl;

import com.micro.fast.common.service.impl.SsmServiceImpl;
import com.micro.fast.upms.dao.UpmsUserOrganizationMapper;
import com.micro.fast.upms.pojo.UpmsUserOrganization;
import com.micro.fast.upms.service.UpmsUserOrganizationService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author lsy
*/
@Service
public class UpmsUserOrganizationServiceImpl  extends SsmServiceImpl<UpmsUserOrganization,Integer,UpmsUserOrganizationMapper>
implements UpmsUserOrganizationService<UpmsUserOrganization,Integer>,InitializingBean {

  @Autowired
  private UpmsUserOrganizationMapper mapper;

  /**
  * 在这个bean初始化完成后覆盖父类的mapper
  */
  @Override
  public void afterPropertiesSet() throws Exception {
    super.setMapper(this.mapper);
  }
}
