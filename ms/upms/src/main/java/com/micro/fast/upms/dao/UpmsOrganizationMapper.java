package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsOrganization;

public interface UpmsOrganizationMapper extends SsmMapper<UpmsOrganization,Integer> {
    int deleteByPrimaryKey(Integer id);

    int insert(UpmsOrganization record);

    int insertSelective(UpmsOrganization record);

    UpmsOrganization selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UpmsOrganization record);

    int updateByPrimaryKey(UpmsOrganization record);
}