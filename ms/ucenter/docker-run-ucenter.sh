#!/usr/bin/env bash
docker run -d   -p 10010:10010 -p 10011:10011 registry.cn-hangzhou.aliyuncs.com/lishouyu/hub:ms-ucenterV0.0.1-SNAPSHOT  \
 java -jar \
 -Deureka.client.serviceUrl.defaultZone=http://micro:fast@172.17.0.1:10002/eureka/,http://micro:fast@172.17.0.1:10004/eureka/ \
 -Deureka.instance.prefer-ip-address=true \
 -Deureka.instance.ip-address=172.17.0.1 \
 -Dspring.cloud.config.profile=deploy \
 /workhome/app.jar